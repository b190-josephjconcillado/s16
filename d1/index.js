console.log("hello world");

let x = 1397;
let y = 7831;

let sum = x + y;
console.log(sum);

let difference = x - y;
console.log(difference);

let product = x * y;
console.log(product);

let quotient = x / y;
console.log(quotient);

let remainder = y % x;
console.log(remainder);

let assignmentNumber = 8;

//assignmentNumber = assignmentNumber + 2;
assignmentNumber += 2;
console.log(assignmentNumber);
//subtraction
assignmentNumber -= 2;
console.log(assignmentNumber);
//multiplication
assignmentNumber *= 2;
console.log(assignmentNumber);
//division
assignmentNumber /= 2;
console.log(assignmentNumber);
//MDAS multiple operators follows PEMDAS (Parenthesis,Exponent,Multiplication,Division,Addition,Subtraction)
//
let mdas = 1 + 2 - 3 * 4 / 5;
console.log(mdas);

let pemdas = 1 + (2 - 3) * 4 / 5;
console.log(pemdas);

pemdas = (1 + (2 - 3)) * (4 / 5);
console.log(pemdas);

//increment and decrement section
let z = 1;

let increment = ++z;
console.log("Result of pre-incement: " + increment);
console.log("Result of pre-incement: " + z);

z = 1;
increment = z++;
console.log("Result of post-incement: " + increment);
console.log("Result of post-incement: " + z);

//z = 100;

let decrement = --z;
console.log("Result of pre-decrement: " + decrement);
console.log("Result of pre-decrement: " + z);

decrement = z--;
console.log("Result of post-decrement: " + decrement);
console.log("Result of post-decrement: " + z);

//type coercion is the automatic / implicit conversion of values from one data type to another
//this happens when operations are performed on different data types that would normally not be possible and yield irregular result.
//values are automatically assigned/converted from one data type to another in order to resolve operations

let numbA = '10';
let numbB = 12;
let coercion = numbA + numbB;
console.log(coercion);

//try to have type coercion for the following
//number data + number data
//boolean + number
//boolean + string

numbA = 10;
numbB = 20;
coercion = numbA + numbB;
console.log(coercion);
console.log(typeof coercion);

let bools = true;
coercion = bools + numbB;
console.log(coercion);
console.log(typeof coercion);

let string = " The quick brown fox jumps over the lazy dog.";
coercion = bools + string;
console.log(coercion);
console.log(typeof coercion);

//comparison operators

console.log(1 == 1);
console.log(1 == 2);
console.log("-----");

console.log(1 == "1");
console.log(1 == true);
console.log('juan' == 'juan');
let juan = 'juan';
console.log('juan' == juan);
console.log("-----");

//inequality operator

console.log(1 != 1);
console.log(1 != 2);
console.log(1 != '1');
console.log(0 != false);
console.log("juan" != 'juan');
console.log('juan' != juan);
console.log("-----");

// strict equality/inequality

console.log(1 === 1);
console.log(1 === 2);
console.log(1 === '1');
console.log(0 === false);
console.log("juan" === 'juan');
console.log('juan' === juan);
console.log("-----");

//strict inequality operator

console.log(1 !== 1);
console.log(1 !== 2);
console.log(1 !== '1');
console.log(0 !== false);
console.log("juan" !== 'juan');
console.log('juan' !== juan);
console.log("-----");

//relational operators

let a = 50;
let b = 65;

let greaterThan = a > b;
let lessThan = a < b;
let greaterThanOrEqualTo = a >= b;
let lessThanOrEqualTo = a <= b;

console.log(greaterThan);
console.log(lessThan);
console.log(greaterThanOrEqualTo);
console.log(lessThanOrEqualTo);
console.log("-----");

let numStr = "30";
console.log(a>numStr);
string = "twenty";
console.log(b >= string);
console.log("-----");

//logical Operators
let isLegalAge = true;
let isRegistered = false;
//And Operator

let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of and operator: "+ allRequirementsMet);
console.log("-----");

//Or operator
allRequirementsMet = isLegalAge || isRegistered;
console.log("Result of or operator: "+ allRequirementsMet);
console.log("-----");


